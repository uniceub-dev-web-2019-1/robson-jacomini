package main

import (
	"fmt"
	"net/http"
)

func headersToStr(header http.Header) string {
	headersStr := "\t"
	for name, values := range header {
		// Loop over all values for the name.
		for _, value := range values {
			headersStr = headersStr + name + ":" + value + "\n\t"
		}
	}
	return headersStr
}

// processa requisicoes do servidor na rota principal
func serverHandler(w http.ResponseWriter, r *http.Request) {
	/* Preenche dados:
	- Método
	- Path completo (nome do protocolo, ip e porta, uri e query-string)
	- Cabeçalho (headers)
	*/
	if r.TLS != nil {
		r.URL.Scheme = "https"
	} else {
		r.URL.Scheme = "http"
	}
	query := ""
	if r.URL.RawQuery != "" {
		query = "?" + r.URL.RawQuery
	}
	//       Protocolo           ip e porta    uri      query-string
	path := r.URL.Scheme + "://" + r.Host + r.URL.Path + query
	headers := headersToStr(r.Header)
	requestInfo := "Método: " + r.Method + "\nPath: " + path + "\nCabeçalho: \n" + headers

	fmt.Println(requestInfo)
	w.Write([]byte(requestInfo))
}

func main() {
	port := "8080"
	// registrando handler para rota principal
	http.HandleFunc("/", serverHandler)

	fmt.Println("Starting Server... running on port:" + port)
	// iniciando servidor na porta 8080
	err := http.ListenAndServe(":"+port, nil).Error()
	if err != "" {
		fmt.Println("Error creating server - " + err)
	}
}
