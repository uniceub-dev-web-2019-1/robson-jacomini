package main

import (
	"fmt"
	"net/http"
	"github.com/gorilla/mux"
)

func getUser(w http.ResponseWriter, r *http.Request){
	params := r.URL.Query()
	if(len(params["name"]) > 0){
		name := params["name"][0]
		w.Write([]byte("Get request\n User: "+name))
	}
}

func postUser(w http.ResponseWriter, r *http.Request){
	contType := r.Header["Content-Type"]
	w.Write([]byte("Post request with Content-Type: "+contType[0]))
}

func putUser(w http.ResponseWriter, r *http.Request){
	contType := r.Header["Content-Type"]
	if(len(contType) > 0){
		w.Write([]byte("Put Request with Content-Type: "+contType[0]))
	} else {
		w.Write([]byte("Put Request without Content-Type"))
	}
}

func main() {
	port := "8080"
	r := mux.NewRouter()

	r.HandleFunc("/user", getUser).
	Methods("GET").
	Queries("name", "{*}")

	r.HandleFunc("/user", postUser).
	Methods("POST").
	HeadersRegexp("Content-Type", "application/(text|json)")

	r.HandleFunc("/user", putUser).
	Methods("PUT")

	s:=http.Server{
		Addr:":"+port,
		Handler:r,
	}

	fmt.Println("Starting Server... running on http://localhost:" + port)
	err := s.ListenAndServe().Error()
	if err != "" {
		fmt.Println("Error creating server - " + err)
	}
}
