package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/gorilla/mux"
)

type InputData struct {
	Num_1 float32 `json:"num_1"`
	Num_2 float32 `json:"num_2"`
}

type ResultData struct {
	result float32
}

func calculator(w http.ResponseWriter, r *http.Request) {
	// Read body
	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	if len(b) > 0 {
		// Unmarshal
		var input InputData
		err = json.Unmarshal(b, &input)
		if err != nil {
			http.Error(w, err.Error(), 500)
			return
		}
		fmt.Println(b)
		fmt.Printf("%+v", input)

		num1 := input.Num_1
		num2 := input.Num_2
		result := num1 + num2

		w.WriteHeader(200) // statuscode 200
		resultStr := fmt.Sprintf("%.2f", result)

		w.Header().Set("content-type", "application/json")
		w.Write([]byte("{\"result\":" + string(resultStr) + "}"))

	} else {
		w.Write([]byte("body empty"))
		return
	}

}

func getOK(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(200) // statuscode 200
	w.Write([]byte("OK"))
}

func main() {
	port := "8080"
	r := mux.NewRouter()
	// permite somente esses metodos
	// r.Methods("GET", "POST", "DELETE")

	r.HandleFunc("/", getOK)

	r.HandleFunc("/calculator/sum", calculator).
		Methods("POST").
		Headers("Content-Type", "application/json")

	s := http.Server{
		Addr:    ":" + port,
		Handler: r,
	}

	fmt.Println("Starting Server... running on http://localhost:" + port)
	// iniciando servidor na porta 8080
	err := s.ListenAndServe().Error()
	if err != "" {
		fmt.Println("Error creating server - " + err)
	}
}
